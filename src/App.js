import './assets/votestyle.css';
import UnVote from "./UnVote";
import React from 'react';
import axios from 'axios';
import parse from 'html-react-parser';
import { uuid } from 'uuidv4';

//function App() {
class App extends React.Component {

  state = {
    votestate: '',
    compare: 'none',
    id: uuid(),
    checkedCat: parse(''),
    checkedDog: parse(''),
    // URL: 'localhost'
    URL: '3.235.6.15'
  };

  onFormSubmit = async (event) => {
    console.log('onFormSubmit ' + event);
    event.preventDefault();

    //let compare ='none';
    const votex = this.state.votestate;
    const compare = this.state.compare;
    const id = this.state.id;
    console.log('Voting for ' + votex + ' with ID=' + id);
    console.log('#value votestate=' + votex + ' #value compare=' + compare);

    //id == id && already vote
    if (compare !== 'none') {
      console.log('Goto Update');
      //Update
      axios.put(`http://${this.state.URL}:3000/votes/` + id,
        {
          vote: votex
        }).then(response => {
          console.log("response: ", response)
          // do something about response
        })
        .catch(err => {
          console.error(err)
        });
    }
    else {
      console.log('Goto Insert');
      //Insert
      axios.post(`http://${this.state.URL}:3000/votes/`,
        {
          id: id,
          vote: votex
        }
      ).then(response => {
        console.log("response: ", response)
        // do something about response
      })
        .catch(err => {
          console.error(err)
        });
    }

    if (votex === 'cat') {
      this.setState({
        checkedCat: parse('<i class="fa fa-check-circle">'),
        checkedDog: parse('')
      });
    } else {
      this.setState({
        checkedCat: parse(''),
        checkedDog: parse('<i class="fa fa-check-circle">')
      });
    }

    this.setState({
      votestate: votex,
      compare: votex
    });

  }

  onUndoSubmit = async (event) => {
    event.preventDefault();
    console.log("onUndoSubmit");

    this.setState({
      checkedCat: parse(''),
      checkedDog: parse(''),
      compare: 'none'
    });

    const idx = this.state.id;
    console.log('Undo Voting process ID=' + idx);

    axios.delete(`http://${this.state.URL}:3000/votes/` + idx, {})
      .then(response => {
        console.log("response: ", response)
        // do something about response
      })
      .catch(err => {
        console.error(err)
      });
  }

  render() { //render
    return (

      <div id="content-container">
        <div id="content-container-center">

          <h1>[Good Day]</h1>
          <h2>Vote Your Favourite Pet!</h2>
          <h3>Cat vs Dog!</h3>
          <form id="choice" name='form' method="POST" onClick={e => this.setState({ votestate: e.target.value })} onSubmit={this.onFormSubmit}>
            <button id="b" type="submit" name="vote" className="a" value="cat">Cats {this.state.checkedCat} </button>
            <button id="a" type="submit" name="vote" className="b" value="dog">Dogs {this.state.checkedDog}</button>
          </form>
          <div id="tip">
            (Tip: you can change your vote)
        </div>
          <br />

          <div id="hostname">
            Processed by session ID {this.state.id}
          </div>

          <br />

          <form id="choice" name='form' method="POST" onClick={this.onUndoSubmit} >
            <UnVote> Vote Me</UnVote>
          </form>
        </div>
      </div>
    );
  }
} ///render
export default App;
